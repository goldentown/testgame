﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Game.Api
{
    public class ComputerChoiceController : ApiController
    {
        // GET api/<controller>
        public double Get() => new Random().NextDouble();

    }
}